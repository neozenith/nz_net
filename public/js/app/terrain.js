define(
['threecore'],
function(THREE){


  function Terrain(WORLD_WIDTH, WORLD_DEPTH, VERT_RES) {
    var geometry = new THREE.PlaneBufferGeometry( WORLD_WIDTH, WORLD_DEPTH, VERT_RES, VERT_RES );
    //initially plane is flat wall facing camera
    //rotate around X axis to roll it back onto flat ground
    geometry.applyMatrix( new THREE.Matrix4().makeRotationX( - Math.PI / 2 ) );
     

    var material = new THREE.MeshPhongMaterial( 
      {
        wireframe: true, 
        color: 0x004400
      } 
    );


    terrain = new THREE.Mesh(geometry, material);
    terrain.receiveShadow = true;
    return terrain; 
  }

  return Terrain;
}
);



