define(
["three", "scene"],
function LightScene(THREE, scene) {

  //LIGHTS
  sun = new THREE.PointLight( 0xc0c0c0 ); // soft white light
  sun.position.set( 1.0, WORLD_HEIGHT, 1.0);
  scene.add(sun);

  var d = 0.5*WORLD_WIDTH;
  var s = 0.125*WORLD_WIDTH;

  sun_shadow = new THREE.DirectionalLight( 0xc0c0c0, 0.5);
  sun_shadow.position.set(0, 0.5*WORLD_HEIGHT, 0);
 
  sun_shadow.shadowCameraVisible = SHADOW_CAMERA_DEBUG;

  sun_shadow.shadowCameraNear = 1;
  sun_shadow.shadowCameraFar = WORLD_DEPTH;
//*/ 

  sun_shadow.shadowCameraLeft = -d;
  sun_shadow.shadowCameraRight = d;
  sun_shadow.shadowCameraTop = d;
  sun_shadow.shadowCameraBottom = -d;
//*/
  sun_shadow.shadowMapWidth = s;
  sun_shadow.shadowMapHeight = s;
//*/
  
  sun_shadow.castShadow = true;
  sun_shadow.shadowOnly = true;
  sun_shadow.target = terrain;
  scene.add(sun_shadow);

  renderer.shadowMapType = THREE.PCFSoftShadowMap;
  renderer.shadowMapEnabled = true;
  renderer.shadowMapSoft = true;  

}


function load_models(scene){
  // instantiate a loader
  var loader = new THREE.OBJMTLLoader();
  
  // load a resource
  loader.load(
  // resource URL
    'models/HN48FlyingCar/HN48FlyingCar/HN48FlyingCar.obj',
    'models/HN48FlyingCar/HN48FlyingCar/HN48FlyingCar.mtl',
      // Function when resource is loaded
    function ( object ) {
      object.traverse( function( node ) { if ( node instanceof THREE.Mesh ) { node.castShadow = true; } } );

      flyingcar = object;
      //flyingcar.scale.set(0.05, 0.05, 0.05);
      flyingcar.position.set(0, 2, -5);
      scene.add( flyingcar );
    },
    //function for download progress...
    loading_progress
  );
}

function loading_progress(progress) {
  console.log('loading...'+progress.loaded+'::'+progress.total);
}

function update_scene(scene) {
  
  if (flyingcar)
  {
    
    var move_speed = 10.0;
    var turn_speed = 0.07;
    
    if (keyboard.pressed("w") || keyboard.pressed("W")) {
      flyingcar.translateZ(move_speed);
    } 
    if (keyboard.pressed("s") || keyboard.pressed("S")) {
      flyingcar.translateZ(-move_speed);
    } 
    if (keyboard.pressed("a") || keyboard.pressed("A")) {
      flyingcar.rotateOnAxis(flyingcar.up,  turn_speed);
    } 
    if (keyboard.pressed("d") || keyboard.pressed("D")) {
      flyingcar.rotateOnAxis(flyingcar.up,  -turn_speed);
    }
    if (keyboard.pressed("q")) {
      flyingcar.rotateOnAxis(new THREE.Vector3(1,0,0),  turn_speed);
    } 
    if (keyboard.pressed("e")) {
      flyingcar.rotateOnAxis(new THREE.Vector3(1,0,0),  -turn_speed);
    }

    flyingcar.position.y = 200;
    // CHASE CAMERA
    var relativeCameraOffset = new THREE.Vector3(0,300,-500);
    var cameraOffset = relativeCameraOffset.applyMatrix4( flyingcar.matrixWorld );
    camera.position.x = cameraOffset.x;
    camera.position.y = cameraOffset.y;
    camera.position.z = cameraOffset.z;
    camera.lookAt( flyingcar.position );
  } 
  
}
