define(
['threecore', 'app/terrain'],
function(THREE, Terrain) {

  var scene = new THREE.Scene();
  
  //TERRAIN
  //scene.add(new Terrain(5120, 5120, 64) );

  // SKYBOX/FOG
  var skyBoxGeometry = new THREE.BoxGeometry( 5120, 0.5*5120, 5120 );
  var skyBoxMaterial = new THREE.MeshLambertMaterial( 
    { 
      color: 0x7ec0ee, 
      side: THREE.BackSide,
      wireframe: true
    } 
  );
  var skyBox = new THREE.Mesh( skyBoxGeometry, skyBoxMaterial );
  skyBox.position.set(0, 0.25*5120-5.0, 0);
  scene.add(skyBox);
  scene.fog = new THREE.FogExp2( 0x9999ff, 0.00025 );

  //LIGHTING
  
  //MODELS
/*
  function load_models(scene){
    // instantiate a loader
    var loader = new THREE.OBJMTLLoader();
    
    // load a resource
    loader.load(
    // resource URL
      //'models/Flyer/Flyer.obj',
      //'models/Flyer/Flyer.mtl',
      'models/HN48FlyingCar/HN48FlyingCar/HN48FlyingCar.obj',
      'models/HN48FlyingCar/HN48FlyingCar/HN48FlyingCar.mtl',
        // Function when resource is loaded
      function ( object ) {
        object.traverse( function( node ) { if ( node instanceof THREE.Mesh ) { node.castShadow = true; } } );

        flyingcar = object;
        //flyingcar.scale.set(0.05, 0.05, 0.05);
        flyingcar.position.set(0, 2, -5);
        scene.add( flyingcar );
      },
    );
  };
*/

  return scene;
}
);
