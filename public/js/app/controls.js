
function init(){

  keyboard = new THREEx.KeyboardState();

  window.addEventListener( 'resize', onWindowResize, false );

}

function onWindowResize() {

  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize( window.innerWidth, window.innerHeight );
}


function update_scene(scene) {
  
  if (flyingcar)
  {
    
    var move_speed = 10.0;
    var turn_speed = 0.07;
    
    if (keyboard.pressed("w") || keyboard.pressed("W")) {
      flyingcar.translateZ(move_speed);
    } 
    if (keyboard.pressed("s") || keyboard.pressed("S")) {
      flyingcar.translateZ(-move_speed);
    } 
    if (keyboard.pressed("a") || keyboard.pressed("A")) {
      flyingcar.rotateOnAxis(flyingcar.up,  turn_speed);
    } 
    if (keyboard.pressed("d") || keyboard.pressed("D")) {
      flyingcar.rotateOnAxis(flyingcar.up,  -turn_speed);
    }
    if (keyboard.pressed("q")) {
      flyingcar.rotateOnAxis(new THREE.Vector3(1,0,0),  turn_speed);
    } 
    if (keyboard.pressed("e")) {
      flyingcar.rotateOnAxis(new THREE.Vector3(1,0,0),  -turn_speed);
    }

    flyingcar.position.y = 200;
    // CHASE CAMERA
    var relativeCameraOffset = new THREE.Vector3(0,300,-500);
    var cameraOffset = relativeCameraOffset.applyMatrix4( flyingcar.matrixWorld );
    camera.position.x = cameraOffset.x;
    camera.position.y = cameraOffset.y;
    camera.position.z = cameraOffset.z;
    camera.lookAt( flyingcar.position );
  } 
  
}
