define(
['threecore', 'app/container'],
function(THREE, container){
  container.innerHTML = "";
  
  var renderer = new THREE.WebGLRenderer( { clearColor: 0x000000 } );
  renderer.sortObjects = false;
  renderer.autoClear = false;
  renderer.setSize( window.innerWidth, window.innerHeight );
  renderer.setClearColor( 0xbfd1e5 );
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.shadowMapType = THREE.PCFSoftShadowMap;
  renderer.shadowMapEnabled = true;
  renderer.shadowMapSoft = true;  
  
  container.appendChild( renderer.domElement );

  var updateSize = function () {
    renderer.setSize( container.offsetWidth, container.offsetHeight );
  };
  window.addEventListener( 'resize', updateSize, false );
  updateSize();

  return renderer;
}
);
