
// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
  console.log('statusChangeCallback');
  console.log(response);
  
  if (response.status === 'connected') {
    // Logged into your app and Facebook.
    console.log(response.authResponse.accessToken);
    fb_authenticated();
  } else if (response.status === 'not_authorized') {
    // The person is logged into Facebook, but not your app.
    document.getElementById('fb_login_status').innerHTML = 'Please log ' +
    'into this app.';
    //appLogout();
  } else {
    // The person is not logged into Facebook, so we're not sure if
    // they are logged into this app or not.
    document.getElementById('fb_login_status').innerHTML = 'Please log ' +
    'into Facebook.';
    //appLogout();
  }
}

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
}


// Load the SDK asynchronously
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function fb_authenticated() {
  console.log('Welcome!  Fetching your information.... ');

  FB.api('/me?fields=id,name,email,picture', function(response) {
    console.log('Successful login for: ' + response.name);
    console.log(JSON.stringify(response));
    if (response && !response.error){
      var profile_pic = $('<img style="border-radius:50%; height:4em;" '+
        'src="'+response.picture.data.url+'" '+
        'alt="'+response.name + ' <'+response.email+'> '+'"/>'+
        '<a href="#" onclick="fb_signout();">Sign out</a>'
        );
      var stat = $('#fb_login_status');
      stat.prepend(profile_pic);
    }

  });

  /* make the API call */
  /*
  FB.api(
    "/me/friends",
    function (response) {
      if (response && !response.error) {
        console.log('FRIENDS');
        console.log(response);
        var friend_list = response.data;
        var stat = $('#status');
        var fl = $('<ul id="friends"></ul>');
        for (var i = 0; i < friend_list.length; i++){
          fl.append('<li>'+
            '<img style="border-radius:50%;" '+
            'src="http://graph.facebook.com/'+friend_list[i].id+'/picture"'+
            ' /> '+friend_list[i].name+
            '</li>');
        }
        //stat.append('<div> Friends List:</div>');
        //stat.append(fl);

      }
    }
  );
  */

}
function fb_signout() {
  FB.logout(function () {
    console.log('User signed out.');
    $('#fb_login_status').html("");
    checkLoginState();
  });
}


function google_authenticated(googleUser) {
  var profile = googleUser.getBasicProfile();
  var name = profile.getName();
  var email = profile.getEmail();
  var img_url = profile.getImageUrl();
  
  console.log('Successful login for: ' + name);
  var profile_pic = $('<img style="border-radius:50%; height:4em;" '+
      'src="'+img_url+'" '+
      'alt="'+name + ' <'+email+'> "/>'+
      '<a href="#" onclick="google_signout();">Sign out</a>'
    );
  var stat = $('#google_login_status');
  stat.prepend(profile_pic);

}

function google_signout() {
  var auth2 = gapi.auth2.getAuthInstance();
  auth2.signOut().then(function () {
    console.log('User signed out.');
    $('#google_login_status').html("");
  });
}
