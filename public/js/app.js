define(
[
  'threecore', 
  'app/container', 
  'app/renderer', 
  'app/scene',
  'app/camera'
],
function(
  THREE, 
  container, renderer, scene, camera
) {

  var N = 5120;
  var WORLD_WIDTH = N;
  var WORLD_DEPTH = N;
  var WORLD_HEIGHT = N;
  var VERT_RES = 64.0;

  var APP = {
    models: [],
    init: function (){
      //LIGHTS
      var sun = new THREE.PointLight( 0xc0c0c0 ); // soft white light
      sun.position.set( 1.0, WORLD_HEIGHT, 1.0);
      scene.add(sun);

      var d = 0.5*WORLD_WIDTH;
      var s = 0.125*WORLD_WIDTH;

      var sun_shadow = new THREE.DirectionalLight( 0xc0c0c0, 0.5);
      sun_shadow.position.set(0, 0.5*WORLD_HEIGHT, 0);
     
      sun_shadow.shadowCameraVisible = true; //SHADOW_CAMERA_DEBUG;

      sun_shadow.shadowCameraNear = 1;
      sun_shadow.shadowCameraFar = this.WORLD_DEPTH;

      sun_shadow.shadowCameraLeft = -d;
      sun_shadow.shadowCameraRight = d;
      sun_shadow.shadowCameraTop = d;
      sun_shadow.shadowCameraBottom = -d;
      sun_shadow.shadowMapWidth = s;
      sun_shadow.shadowMapHeight = s;
      
      sun_shadow.castShadow = true;
      sun_shadow.shadowOnly = true;
      //sun_shadow.target = terrain;
      scene.add(sun_shadow);


/*
      this.keyboard = new THREEx.KeyboardState();

      window.addEventListener( 'resize', this.onWindowResize, false );
*/
    },

    animate: function () {
      requestAnimationFrame( APP.animate );

      //update(scene);
      renderer.render( scene, camera );
  
    },
    update: function(scene){

      if (flyingcar)
      {
        
        var move_speed = 10.0;
        var turn_speed = 0.07;
        
        if (keyboard.pressed("w") || keyboard.pressed("W")) {
          flyingcar.translateZ(move_speed);
        } 
        if (keyboard.pressed("s") || keyboard.pressed("S")) {
          flyingcar.translateZ(-move_speed);
        } 
        if (keyboard.pressed("a") || keyboard.pressed("A")) {
          flyingcar.rotateOnAxis(flyingcar.up,  turn_speed);
        } 
        if (keyboard.pressed("d") || keyboard.pressed("D")) {
          flyingcar.rotateOnAxis(flyingcar.up,  -turn_speed);
        }
        if (keyboard.pressed("q")) {
          flyingcar.rotateOnAxis(new THREE.Vector3(1,0,0),  turn_speed);
        } 
        if (keyboard.pressed("e")) {
          flyingcar.rotateOnAxis(new THREE.Vector3(1,0,0),  -turn_speed);
        }

        flyingcar.position.y = 200;
        // CHASE CAMERA
        var relativeCameraOffset = new THREE.Vector3(0,300,-500);
        var cameraOffset = relativeCameraOffset.applyMatrix4( flyingcar.matrixWorld );
        camera.position.x = cameraOffset.x;
        camera.position.y = cameraOffset.y;
        camera.position.z = cameraOffset.z;
        camera.lookAt( flyingcar.position );
      } 

    }
  };
  return APP;
}
);
