var canvas;
var gl;
var program;
var redraw = true;
var last_frame = Date.now();
var wireframe = true;
var vBuffers = [];
var cBuffers = [];
var objects = [];
var scene = [];
var camera = null;
var textures = [
  '/images/textures/dry_mud.jpg',
  '/images/textures/lava.jpg',
  '/images/textures/crate.gif',
  '/images/textures/pepsi.jpg',
  '/images/textures/earth.jpg'
];

var GL_UTILS = (function () {
  'use strict';
  var _M = {};

  _M.initGL = function initGL(canvas) {
    var gl;
    try {
      gl = canvas.getContext('experimental-webgl');
    } catch (e) {
      console.log('ERROR: Failed to init canvas with error msg: ' + e);
    }

    if (!gl) {
      alert('Could not initialise WebGL, sorry :-(');
    }

    return gl;
  };

  _M.getShader = function getShader(gl, id) {
    var shaderScript = document.getElementById(id);
    if (!shaderScript) {
      return null;
    }

    var str = '';
    var k = shaderScript.firstChild;
    while (k) {
      if (k.nodeType == 3) {
        str += k.textContent;
      }

      k = k.nextSibling;
    }

    var shader;
    if (shaderScript.type == 'x-shader/x-fragment') {
      shader = gl.createShader(gl.FRAGMENT_SHADER);
    } else if (shaderScript.type == 'x-shader/x-vertex') {
      shader = gl.createShader(gl.VERTEX_SHADER);
    } else {
      return null;
    }

    gl.shaderSource(shader, str);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
      alert(gl.getShaderInfoLog(shader));
      return null;
    }

    return shader;
  };

  _M.initShaders = function initShaders(gl, vertexShaderID, fragmentShaderID) {
    var program;

    //Fetch shaders from DOM
    var fragmentShader = this.getShader(gl, fragmentShaderID);
    var vertexShader = this.getShader(gl, vertexShaderID);

    //Create empty shader program
    program = gl.createProgram();

    //Build shaders into program
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
    gl.linkProgram(program);

    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
      alert('Could not initialise shaders');
    }

    gl.useProgram(program);

    //MANDATORY
    //Enable Vertex Array Buffer so the Shader program knows how it is
    //referenced in the shader program.
    program.vertexPositionAttribute = gl.getAttribLocation(program, 'vPosition');
    gl.enableVertexAttribArray(program.vertexPositionAttribute);

    //Color Vertices
    program.vertexColorAttribute = gl.getAttribLocation(program, 'vColor');
    gl.enableVertexAttribArray(program.vertexColorAttribute);

    //Texture Coord per vertex
    program.vertexTextureCoordAttribute = gl.getAttribLocation(program, 'vTexCoord');
    gl.enableVertexAttribArray(program.vertexTextureCoordAttribute);

    program.useTextureUniform = gl.getUniformLocation(program, 'uUseTexture');
    program.textureUniform = gl.getUniformLocation(program, 'uTexture');

    program.pMatrixUniform = gl.getUniformLocation(program, 'uPMatrix');
    program.mvMatrixUniform = gl.getUniformLocation(program, 'uMVMatrix');

    return program;
  };

  return _M;
}());

/* SHAPES Module
 * Primitive - 3D Primitive Base Class
 * Abstraction of common rendering code for 3D primitives
 * */
var SHAPES = (function SHAPES() {
  'use strict';
  var _M = {}; //M for module
  _M.Primitive = function Primitive(config) {
    var _obj = {};
    var debug = false;

    //TODO: Primitive - feature development
    // [] Apply Texture through shader.

    if (debug) {
      console.log('Primitive');
    }

    if (config === null) {
      config = {};
    }

    _obj.position     = config.position || vec3.fromValues(0, 0, 0);
    _obj.orientation  = config.orientation || vec3.fromValues(0, 0, 0);
    _obj.scale        = config.scale || vec3.fromValues(1, 1, 1);
    _obj.color        = config.color || vec4.fromValues(0.5, 0.5, 0.5, 1.0);
    _obj.wireframe = config.wireframe || true;
    _obj.wireframe_color  = config.wireframe_color || vec4.fromValues(0.0, 0.0, 1.0, 1.0);

    _obj.texture_url = config.texture || null;
    _obj.useTexture = false; //only set true once it has loaded
    _obj.texture = null;

    // Buffers:
    // v  - vertices
    // c  - colors per vertex
    // i  - indices (to reuse references to same vertex and reduce vBuffer size)
    // tc - texture coordinate
    _obj.vBuffer = null;
    _obj.cBuffer = null;
    _obj.iBuffer = null;
    _obj.tcBuffer = null;
    _obj.wf_cBuffer = null; //wireframe
    _obj.wf_iBuffer = null; //wireframe

    _obj.points = []; //implementation must implement this
    _obj.colors = []; //implementation must implement this
    _obj.indices = []; //implementation must implement this
    _obj.texCoords = []; //implementation must implement this

    _obj.wf_colors = [];
    _obj.wf_indices = []; //implementation must implement this

    _obj.vertexSize = 4;
    _obj.vertexCount = _obj.points.length;
    _obj.gl_draw_primitive = gl.TRIANGLES;
    _obj.gl_wireframe_primitive = gl.LINE_LOOP;

    // initTexture - This is a private/protected method used as
    // a callback for loadTexture
    _obj.initTexture = function initTexture(gl, texture) {
      console.log('_initTexture');
      console.log({
        w: texture.image.width,
        h: texture.image.height,
        src: texture.image.src
      });

      gl.bindTexture(gl.TEXTURE_2D, texture);
      gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
      gl.generateMipmap(gl.TEXTURE_2D);
      gl.bindTexture(gl.TEXTURE_2D, null);

      console.log(_obj.texture);
      _obj.useTexture = true;

    };

    // handleTextureLoad - Callback to handle when image has finished
    // downloading and pass the gl context and texture reference.
    _obj.handleTextureLoad = function () {
      _obj.initTexture(gl, _obj.texture);
    };

    // loadTexture - Async fetch image
    _obj.loadTexture = function loadTexture(gl, textureUrl) {
      if (textureUrl === null || textureUrl.length <= 0){
        _obj.useTexture = false;
        return;
      }

      _obj.texture = gl.createTexture();
      gl.bindTexture(gl.TEXTURE_2D, _obj.texture);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA,
          gl.UNSIGNED_BYTE, new Uint8Array([255, 0, 0, 255]));
      _obj.texture.image = new Image();
      _obj.texture.image.onload = _obj.handleTextureLoad;
      _obj.texture.image.src = textureUrl;
    };

    _obj.applyColor = function applyColor() {

      // Apply this.color to all vertices
      this.colors = [];
      this.wf_colors = [];
      for (var i = 0; i < this.vertexCount; i++) {
        this.colors.push(this.color[0]);
        this.colors.push(this.color[1]);
        this.colors.push(this.color[2]);
        this.colors.push(this.color[3]);
        this.wf_colors.push(this.wireframe_color[0]);
        this.wf_colors.push(this.wireframe_color[1]);
        this.wf_colors.push(this.wireframe_color[2]);
        this.wf_colors.push(this.wireframe_color[3]);
      }
    };

    /* Model View Matrix - Takes in the object's position, orientation and scale
     * and generates the Model View Matrix to position this object in space.
     * */
    _obj.mvMatrix = function mvMatrix() {
      //TODO: persist this so it doesn't recalculate all the time
      var mv = mat4.create();
      mat4.translate(mv, mv, this.position);
      mat4.rotateX(mv, mv, this.orientation[0]);
      mat4.rotateY(mv, mv, this.orientation[1]);
      mat4.rotateZ(mv, mv, this.orientation[2]);
      mat4.scale(mv, mv, this.scale);
      return mv;
    };

    /* Update - Update vertexes and load data into GPU
     * Should only call this when there has been a change to the vertices.
     * @param gl - WebGL context
     * */
    _obj.update = function update(gl) {
      if (debug) {
        console.log('updatePrimitive');
        console.log(this);
      }

      var VERTEX_SIZE = this.vertexSize;
      var vBuffer = this.vBuffer;
      var cBuffer = this.cBuffer;
      var iBuffer = this.iBuffer;
      var tcBuffer = this.tcBuffer;

      var wf_cBuffer = this.wf_cBuffer;
      var wf_iBuffer = this.wf_iBuffer;

      if (vBuffer === null) {
        vBuffer = gl.createBuffer();
        this.vBuffer = vBuffer;
      }

      if (cBuffer === null) {
        cBuffer = gl.createBuffer();
        this.cBuffer = cBuffer;
      }

      if (wf_cBuffer === null) {
        wf_cBuffer = gl.createBuffer();
        this.wf_cBuffer = wf_cBuffer;
      }

      if (iBuffer === null) {
        iBuffer = gl.createBuffer();
        this.iBuffer = iBuffer;
      }

      if (wf_iBuffer === null) {
        wf_iBuffer = gl.createBuffer();
        this.wf_iBuffer = wf_iBuffer;
      }

      if (tcBuffer === null) {
        tcBuffer = gl.createBuffer();
        this.tcBuffer = tcBuffer;
      }

      //Load into GPU
      gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.points), gl.STATIC_DRAW);
      vBuffer.itemSize = VERTEX_SIZE;
      vBuffer.numItems = this.points.length / VERTEX_SIZE;

      //Load into GPU
      gl.bindBuffer(gl.ARRAY_BUFFER, cBuffer);
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.colors), gl.STATIC_DRAW);
      cBuffer.itemSize = VERTEX_SIZE;
      cBuffer.numItems = this.colors.length / VERTEX_SIZE;

      //Load into GPU
      gl.bindBuffer(gl.ARRAY_BUFFER, wf_cBuffer);
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.wf_colors), gl.STATIC_DRAW);
      wf_cBuffer.itemSize = VERTEX_SIZE;
      wf_cBuffer.numItems = this.wf_colors.length / VERTEX_SIZE;

      //Load into GPU
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iBuffer);
      gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(this.indices), gl.STATIC_DRAW);
      iBuffer.itemSize = 1;
      iBuffer.numItems = this.indices.length;

      //Load into GPU
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, wf_iBuffer);
      gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(this.wf_indices), gl.STATIC_DRAW);
      wf_iBuffer.itemSize = 1;
      wf_iBuffer.numItems = this.wf_indices.length;

      //Load into GPU
      gl.bindBuffer(gl.ARRAY_BUFFER, tcBuffer);
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.texCoords), gl.STATIC_DRAW);
      tcBuffer.itemSize = 2;
      tcBuffer.numItems = this.texCoords.length / tcBuffer.itemSize;

      //This is asynchronous so best to have loaded a default object and
      //let the texture load in it's own good time.
      this.loadTexture(gl, this.texture_url);
    };

    /* render - Rendering and animation function
     * @param gl - WebGL Context
     * @param program - the WebGL program to render with.
     * @param t - time in milliseconds since start of animation sequence.
     * */
    _obj.render = function render(gl, program, t){
      if (debug){
        console.log('RenderPrimitive');
        console.log(this);
      }

      var vBuffer = this.vBuffer;
      var cBuffer = this.cBuffer;
      var iBuffer = this.iBuffer;
      var tcBuffer = this.tcBuffer;
      var wf_cBuffer = this.wf_cBuffer;
      var wf_iBuffer = this.wf_iBuffer;

      gl.uniform1i(program.uUseTexture, false);

      gl.uniformMatrix4fv(program.mvMatrixUniform, false, this.mvMatrix());

      gl.bindBuffer(gl.ARRAY_BUFFER, cBuffer);
      gl.vertexAttribPointer(program.vertexColorAttribute, cBuffer.itemSize, gl.FLOAT, false, 0, 0);
      gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
      gl.vertexAttribPointer(program.vertexPositionAttribute, vBuffer.itemSize, gl.FLOAT, false, 0, 0);
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, iBuffer);

      gl.uniform1i(program.uUseTexture, this.useTexture);
      if (_obj.useTexture) {
        gl.bindBuffer(gl.ARRAY_BUFFER, tcBuffer);
        gl.vertexAttribPointer(program.vertexTextureCoordAttribute, tcBuffer.itemSize, gl.FLOAT, false, 0, 0);
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, this.texture);
        gl.uniform1i(program.uTexture, 0); //Default Texture index
      }

      gl.drawElements(this.gl_draw_primitive, iBuffer.numItems, gl.UNSIGNED_SHORT, 0);

      if (this.wireframe){
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, wf_iBuffer);
        gl.bindBuffer(gl.ARRAY_BUFFER, wf_cBuffer);
        gl.vertexAttribPointer(program.vertexColorAttribute, wf_cBuffer.itemSize, gl.FLOAT, false, 0, 0);
        gl.drawElements(this.gl_wireframe_primitive, wf_iBuffer.numItems, gl.UNSIGNED_SHORT, 0);
      }
    };

    return _obj;
  };

  _M.Plane = function Plane(config) {
    var obj = this.Primitive(config);
    console.log('GROUND PLANE');
    var segs = config.segments || 4;
    obj.points = [

      // corners
       1,  1, 0, 1,
       1, -1, 0, 1,
      -1, -1, 0, 1,
      -1,  1, 0, 1,

      // center
       0, 0, 0, 1,

      // edge midpoints
       0,  1, 0, 1,
       0, -1, 0, 1,
       1, 0, 0, 1,
      -1, 0, 0, 1
      ];
    obj.texCoords = [

      //corners
      1.0, 1.0,
      1.0, 0.0,
      0.0, 0.0,
      0.0, 1.0,

      // center
      0.5, 0.5,

      //edge midpoints
      0.5, 1.0,
      0.5, 0.0,
      1.0, 0.5,
      0.0, 0.5
    ];
    obj.vertexCount = obj.points.length /  obj.vertexSize;

    obj.applyColor();

    obj.indices = [
      1, 0, 2, 2, 0, 3
      ];
    obj.wf_indices = [
      0, 1, 2, 3, 0, 5, 6, 2, 8, 7, 0
      ];

    return obj;
  };

  _M.Cube = function Cube(config) {
    var obj = this.Primitive(config);
    console.log('CUBE');

    obj.points = [

      // Top
       1,  1,  1, 1,
       1,  1, -1, 1,
      -1,  1, -1, 1,
      -1,  1,  1, 1,

      //Bottom
       1, -1,  1, 1,
       1, -1, -1, 1,
      -1, -1, -1, 1,
      -1, -1,  1, 1
      ];
    obj.texCoords = [
      1.0, 1.0,
      1.0, 0.0,
      0.0, 0.0,
      0.0, 1.0,

      1.0, 1.0,
      1.0, 0.0,
      0.0, 0.0,
      0.0, 1.0
    ];
    obj.vertexCount = obj.points.length /  obj.vertexSize;

    obj.applyColor();

    obj.indices = [
      0, 1, 3,  3, 1, 2,
      2, 1, 6,  6, 1, 5,
      5, 1, 4,  4, 1, 0,
      0, 3, 4,  4, 3, 7,
      7, 3, 6,  6, 3, 2,
      7, 6, 4,  4, 6, 5
      ];
    obj.wf_indices = [
      0, 1, 2, 3, 0,
      4, 5, 6, 7, 4,
      5, 1, 2, 6, 7, 3
      ];

    return obj;
  };

  _M.Sphere = function Sphere(config) {
    var obj = this.Primitive(config);
    console.log('SPHERE');
    var segs = config.segments || 32;
    obj.points = [];
    obj.texCoords = [];

    for (var lat = 0; lat <= segs; lat++){
      for (var lon = 0; lon < segs; lon++){

        var y = lat / segs * Math.PI;
        var x = lon / segs * 2.0 * Math.PI;

        var A = Math.cos(y);
        var B = Math.sin(y);

        var C = B * Math.cos(x);
        var D = B * Math.sin(x);

        obj.points.push(C);
        obj.points.push(D);
        obj.points.push(A);
        obj.points.push(1);

        obj.texCoords.push(1.0 - lon / segs);
        obj.texCoords.push(0.5 * (lat / segs) + 0.25);
      }
    }

    obj.vertexCount = obj.points.length /  obj.vertexSize;

    for (var i = 0; i < segs; i++){
      for (var j = 0; j < segs; j++){

        var r = i * segs; //start of this ring

        var s = (i + 1) * segs; //start of next ring

        var E = j % segs + r;
        var F = (j + 1) % segs + r;

        var G = j % segs + s;
        var H = (j + 1) % segs + s;

        obj.indices.push(E);
        obj.indices.push(F);
        obj.indices.push(G);

        obj.indices.push(G);
        obj.indices.push(F);
        obj.indices.push(H);

      }
    }

    obj.wf_indices = obj.indices;

    obj.applyColor();

    return obj;
  };

  _M.Cylinder = function Cylinder(config) {
    var obj = this.Primitive(config);
    console.log('CYLINDER');
    var segs = config.segments || 64;

    //Top point
    obj.points.push(0);
    obj.points.push(1);
    obj.points.push(0);
    obj.points.push(1);
    obj.texCoords.push(0.5);
    obj.texCoords.push(1.0);
    var s = 0;
    var x = 0;
    var z = 0;
    for (s = 0; s < segs; s++){
      x = Math.cos(s / segs * 2 * Math.PI);
      z = Math.sin(s / segs * 2 * Math.PI);

      obj.points.push(x);
      obj.points.push(1);
      obj.points.push(z);
      obj.points.push(1);

      obj.texCoords.push(0.5 * (x + 1.0));
      obj.texCoords.push(0.78);
    }

    for (s = 0; s < segs; s++){
      x = Math.cos(s / segs * 2 * Math.PI);
      z = Math.sin(s / segs * 2 * Math.PI);

      obj.points.push(x);
      obj.points.push(-1);
      obj.points.push(z);
      obj.points.push(1);
      obj.texCoords.push(0.5 * (x + 1.0));
      obj.texCoords.push(0.22);
    }

    //Bottom point
    obj.points.push(0);
    obj.points.push(-1);
    obj.points.push(0);
    obj.points.push(1);
    obj.texCoords.push(0.5);
    obj.texCoords.push(0.0);

    obj.vertexCount = obj.points.length /  obj.vertexSize;

    obj.applyColor();

    obj.indices = [];

    for (var i = 0; i < segs; i++){

      var A = (i) % segs + 1;
      var B = (i + 1) % segs + 1;
      var C = A + segs;
      var D = B + segs;

      obj.indices.push(0);
      obj.indices.push(A);
      obj.indices.push(B);

      obj.indices.push(obj.vertexCount - 1);
      obj.indices.push(C);
      obj.indices.push(D);

      obj.indices.push(A);
      obj.indices.push(B);
      obj.indices.push(C);

      obj.indices.push(C);
      obj.indices.push(B);
      obj.indices.push(D);

      obj.wf_indices.push(A);
      obj.wf_indices.push(B);
      obj.wf_indices.push(C);
      obj.wf_indices.push(D);
    }

    return obj;
  };

  return _M;
}());

function webGLStart() {
  // TODO:
  // http://stackoverflow.com/questions/23598471/how-do-i-clean-up-and-unload-a-webgl-canvas-context-from-gpu-after-use
  canvas = document.getElementById('gl-canvas');
  gl = GL_UTILS.initGL(canvas);
  program = GL_UTILS.initShaders(gl, 'vertex-shader', 'fragment-shader');

  scene = initScene(gl);

  camera = Camera();
  camera.position = vec3.fromValues(0, 0, -10);
  camera.orientation = vec3.fromValues(0.40, 0.78, 0);
  camera.resize(); //initialise the size of the camera

  initEventHandlers();

  render();
}

//TODO SCENE - write a SCENE module?
function initScene(gl){
  var scene = [];

  scene.push(
    SHAPES.Cube({
      position: vec3.fromValues(2, 0, 2),
      scale: vec3.fromValues(0.5, 0.5, 0.5),
      color: vec4.fromValues(0.5, 0.0, 0.0, 1.0),
      wireframe_color: vec4.fromValues(0.0, 1.0, 1.0, 1.0),
      texture: textures[2]
    })
  );

  scene.push(
    SHAPES.Cylinder({
        position: vec3.fromValues(-2, 0, 2),
        scale: vec3.fromValues(0.5, 0.8, 0.5),
        color: vec4.fromValues(0.0, 0.5, 0.0, 1.0),
        wireframe_color: vec4.fromValues(1.0, 0.0, 1.0, 1.0),
        texture: textures[3]
      })
  );
  /*
  scene.push(
    SHAPES.Cone({
      position: vec3.fromValues(2, 0, -2),
      scale: vec3.fromValues(0.5, 0.5, 0.5),
      color: vec4.fromValues(0.0, 0.0, 0.5, 1.0),
      wireframe_color: vec4.fromValues(1.0, 1.0, 0.0, 1.0)
    })
    );
  */
  scene.push(
    SHAPES.Sphere({
      position: vec3.fromValues(-2, 0, -2),
      orientation: vec3.fromValues(Math.PI * 0.5, 0, 0),
      scale: vec3.fromValues(1.0, 1.0, 1.0),
      color: vec4.fromValues(0.5, 0.0, 0.5, 1.0),
      wireframe_color: vec4.fromValues(0.0, 1.0, 0.0, 1.0),
      texture: textures[4]
    })
  );

  scene.push(
    SHAPES.Plane({
      position: vec3.fromValues(0, -1, 0),
      orientation: vec3.fromValues(Math.PI * 0.5, 0, 0),
      scale: vec3.fromValues(4, 4, 4),
      color: vec4.fromValues(0.3, 0.3, 0.3, 1.0),
      wireframe_color: vec4.fromValues(1.0, 1.0, 1.0, 1.0),
      texture: textures[0]
    })
  );

  updateBuffers(gl, scene); // this line only needs to be called if there is a change to any of the shapes

  gl.clearColor(0.0, 0.0, 0.0, 1.0);
  gl.enable(gl.DEPTH_TEST);

  return scene;
}
/*
 * updateBuffers - forces all objects to load their data into GPU buffers
 * */
function updateBuffers(gl, objects){
  console.log('updateBuffers');
  for (var n = 0; n < objects.length; n++){
    var object = objects[n];
    console.log('Update Object:' + object);
    object.update(gl);
  }
}
/* drawScene - Calls render on all objects in scene
 * */
function drawScene(gl, program, t, scene, camera) {
  gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
  gl.clearColor(0.0, 0.0, 0.0, 1.0);
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  camera.update();

  for (var b = 0; b < scene.length; b++){
    var object = scene[b];
    object.render(gl, program, t);
  }
}
/* render - main render loop
 * */
function render(t) {
  drawScene(gl, program, t, scene, camera);
  requestAnimationFrame(render);
}

/* Camera - Camera class used to maintain state and camera actions
 */
function Camera(args){
  var debug = false;
  var _obj = {};

  if (!args){
    args = {};
  }

  _obj.position = args.position || vec3.fromValues(0, 0, 0);
  _obj.orientation = args.orientation || vec3.fromValues(0, 0, 0);
  _obj.aspect = args.aspect || 1.0;
  _obj.fov = args.fov || 30.0;
  _obj.near = args.near || 2.0;
  _obj.far = args.far || 10000.0;

  _obj.resize = function resize(){
    // http://webglfundamentals.org/webgl/lessons/webgl-resizing-the-canvas.html
    // https://www.khronos.org/webgl/wiki/HandlingHighDPI
    if (debug) {
      console.log('CAMERA');
      console.log('CANVAS:' + canvas.clientWidth + ', ' + canvas.clientHeight);
      console.log('WINDOW:' + window.innerWidth + ', ' + window.innerHeight);
    }

    var pixelratio = window.devicePixelRatio || 1;
    var w = canvas.clientWidth; //canvas.clientWidth is the dimensions of the
    var h = canvas.clientHeight; //canvas element
    var aspect  =  w / h;

    var W = Math.floor(w * pixelratio);
    var H = Math.floor(h * pixelratio);
    canvas.width = W;
    canvas.height = H;

    gl.viewportWidth = W;
    gl.viewportHeight = H;

    this.aspect = aspect;

    this.update();
  };

  _obj.update = function update(){
    gl.uniformMatrix4fv(program.pMatrixUniform, false, this.pMatrix());
  };

  _obj.pMatrix = function pMatrix(){

    var p = mat4.create();
    mat4.perspective(p, this.fov * Math.PI / 180, this.aspect, this.near, this.far);
    mat4.translate(p, p, this.position);
    mat4.rotateX(p, p, this.orientation[0]);
    mat4.rotateY(p, p, this.orientation[1]);
    mat4.rotateZ(p, p, this.orientation[2]);
    return p;
  };

  return _obj;
}

//TODO: refactor into CONTROLS module
// CONTROLS should be a wrapper for input controls and maintain state
// information, such as KeyRollover and last mouse position.
// Also should implement device orientation and device motion.
//
// Present an API of expected callbacks that pass through state information
// so callbacks don't have to manage state.
function initEventHandlers(){
  var mouseDown = false;
  var last_mx = 0;
  var last_my = 0;
  var last_tx = 0;
  var last_ty = 0;
  var keysPressed = {};
  var debug = false;

  function onResize(){
    camera.resize();
  }

  //MOUSE
  function canvasMouseMove(event){
    var element = event.currentTarget;
    var mx = event.clientX; var my = event.clientY;
    var cx = -1.0 + ((mx - canvas.offsetLeft) / canvas.clientWidth) * 2.0;
    var cy = -1.0 + ((canvas.clientHeight - (my - canvas.offsetTop)) / canvas.clientHeight) * 2.0;

    var last_cx = -1.0 + ((last_mx - canvas.offsetLeft) / canvas.clientWidth) * 2.0;
    var last_cy = -1.0 + ((canvas.clientHeight - (last_my - canvas.offsetTop)) / canvas.clientHeight) * 2.0;

    if (mouseDown){
      camera.orientation[1] += -(cx - last_cx); //movement in x direction rotates around Y
      camera.orientation[0] += (cy - last_cy); //movement in y direction rotates around X
    }

    last_mx = mx;
    last_my = my;
  }

  function canvasMouseUp(event){
    var element = event.currentTarget;
    var mx = event.clientX; var my = event.clientY;
    var cx = -1.0 + ((mx - canvas.offsetLeft) / canvas.clientWidth) * 2.0;
    var cy = -1.0 + ((canvas.clientHeight - (my - canvas.offsetTop)) / canvas.clientHeight) * 2.0;
    mouseDown = false;
  }

  function canvasMouseDown(event){
    var element = event.currentTarget;
    var mx = event.clientX; var my = event.clientY;
    var cx = -1.0 + ((mx - canvas.offsetLeft) / canvas.clientWidth) * 2.0;
    var cy = -1.0 + ((canvas.clientHeight - (my - canvas.offsetTop)) / canvas.clientHeight) * 2.0;
    mouseDown = true;
  }

  function canvasKeyUp(event){
    var element = event.currentTarget;
  }

  function canvasKeyDown(event){
    var element = event.currentTarget;
    switch (event.keyCode){
      case 87:
        camera.position[2] += 0.5;
        break;
      case 65:
        camera.orientation[1] += 0.05;
        break;
      case 83:
        camera.position[2] -= 0.5;
        break;
      case 68:
        camera.orientation[1] -= 0.05;
        break;
    }
  }

  //KEYBOARD
  function canvasKeyPress(event){
    var element = event.currentTarget;
  }

  //SCREEN RESIZE
  window.addEventListener('resize', onResize, false);

  //MOUSE
  canvas.addEventListener('mousemove', canvasMouseMove, false);
  canvas.addEventListener('mousedown', canvasMouseDown, false);
  canvas.addEventListener('mouseup', canvasMouseUp, false);

  //KEYBOARD
  document.addEventListener('keyup', canvasKeyUp, false);
  document.addEventListener('keydown', canvasKeyDown, false);
  document.addEventListener('keypress', canvasKeyPress, false);

  //DEVICE SENSORS
  window.addEventListener('deviceorientation', function (event) {
    if (debug) {
      console.log(event.alpha + ' : ' + event.beta + ' : ' + event.gamma);
    }
    /*
    camera.orientation[0] = event.beta / (Math.PI * 2.0);
    camera.orientation[1] = event.alpha / (Math.PI * 2.0);
    camera.orientation[2] = event.gamma / (Math.PI * 2.0);
    */
    event.preventDefault();
  });

  window.addEventListener('devicemotion', function (event) {
    if (debug){
      console.log(event.acceleration.x + ' m/s2 X');
      console.log(event.acceleration.y + ' m/s2 Y');
      console.log(event.acceleration.z + ' m/s2 Z');
    }
  });

  //TOUCHES
  //https://developer.mozilla.org/en-US/docs/Web/API/Touch_events
  //TODO: Touch events
  // [] Multitouch interactions
  canvas.addEventListener('touchstart', function (event) {
    console.log('touchstart2');
    var touches = event.targetTouches;
    /*
    for (var t = 0; t < touches.length; t++) {
      //console.log('touchStart['+t+']'+touches[t].pageX+','+touches[t].pageY);
    }
    */

    last_tx = touches[0].pageX;
    last_ty = touches[0].pageY;
  }, false);

  canvas.addEventListener('touchmove', function (event) {
    var touches = event.targetTouches;
    /*
    for (var t = 0; t < touches.length; t++) {
      //console.log('touchmove['+t+']'+touches[t].pageX+','+touches[t].pageY);
    }
    */

    var tx = touches[0].pageX; var ty = touches[0].pageY;
    var tcx = -1.0 + ((tx - canvas.offsetLeft) / canvas.clientWidth) * 2.0;
    var tcy = -1.0 + ((canvas.clientHeight - (ty - canvas.offsetTop)) / canvas.clientHeight) * 2.0;

    var last_tcx = -1.0 + ((last_tx - canvas.offsetLeft) / canvas.clientWidth) * 2.0;
    var last_tcy = -1.0 + ((canvas.clientHeight - (last_ty - canvas.offsetTop)) / canvas.clientHeight) * 2.0;

    //console.log('touchMove tc['+tcx+','+tcy+']');
    //console.log('touchMove last_tc['+last_tcx+','+last_tcy+']');
    //console.log('touchMove tcD['+(-(tcx - last_tcx))+','+(tcy-last_tcy)+']');

    if (tx !== undefined && ty !== undefined){
      last_tx = tx;
      last_ty = ty;
      camera.orientation[1] += -(tcx - last_tcx); //movement in x direction rotates around Y
      camera.orientation[0] += (tcy - last_tcy); //movement in y direction rotates around X
    }

    event.preventDefault();
  }, false);

  canvas.addEventListener('touchend', function (event) {
    console.log('touchend');
  }, false);

  //touchcancel - occurs when touch wanders outside element bounds
  canvas.addEventListener('touchcancel', function (event) {
    console.log('touchcancel');
  }, false);
}

