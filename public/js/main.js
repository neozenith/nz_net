// Start the app

requirejs.config ({
  paths:{
      Detector: 'libs/Detector',
      threecore: 'libs/three.min'
  },
  shim: {
    Detector: {exports: 'Detector'},
    threecore: {exports: 'THREE' }
  }
});

require( 
['Detector', 'app', 'app/container'], 
function (Detector,  APP, container ) {
  if ( ! Detector.webgl ) {
    Detector.addGetWebGLMessage();
    container.innerHTML = "";
  }
  /* Initialize our app and start the animation loop 
   * (animate is expected to call itself)
   */ 
  APP.init();
  APP.animate();
} );
