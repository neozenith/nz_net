var tap = require('tap').mochaGlobals();

// var should = require('should');
var request = require('supertest');
var app = require('../app');

describe('test root path', function () {
  it('Returns 200', function (done) {
    request(app)
      .get('/')
      .expect(200, done);
  });

  it('Returns HTML', function (done) {
    request(app)
      .get('/')
      .expect('Content-Type', /html/, done);
  });

  it('Returns expected content', function (done) {
    request(app)
      .get('/')
      .expect(/Generic Startup/, done);
  });
});

describe('test /webgl demos', function () {
  var test_route = '/webgl';

  it('Returns 200',  function (done) {
    request(app)
      .get(test_route)
      .expect(200, done);
  });

  it('Returns HTML', function (done) {
    request(app)
      .get(test_route)
      .expect('Content-Type', /html/, done);
  });

  it('Returns expected canvas element', function (done) {
    request(app)
      .get(test_route)
      .expect(/gl-canvas/, done);
  });
});

describe('test /webgl/texture demos', function () {
  var test_route = '/webgl/texture';

  it('Returns 200',  function (done) {
    request(app)
      .get(test_route)
      .expect(200, done);
  });

  it('Returns HTML', function (done) {
    request(app)
      .get(test_route)
      .expect('Content-Type', /html/, done);
  });

  it('Returns expected canvas element', function (done) {
    request(app)
      .get(test_route)
      .expect(/gl-canvas/, done);
  });
});

describe('test /webgl/multiplayer demos', function () {
  var test_route = '/webgl/multiplayer';

  it('Returns 200',  function (done) {
    request(app)
      .get(test_route)
      .expect(200, done);
  });

  it('Returns HTML', function (done) {
    request(app)
      .get(test_route)
      .expect('Content-Type', /html/, done);
  });

  it('Returns expected canvas element', function (done) {
    request(app)
      .get(test_route)
      .expect(/gl-canvas/, done);
  });
});

describe('test /chat paths', function () {
  it('Returns 200', function (done) {
    request(app)
      .get('/chat')
      .expect(200, done);
  });

  it('Returns HTML', function (done) {
    request(app)
      .get('/chat')
      .expect('Content-Type', /html/, done);
  });
});

describe('test /store path', function () {
  it('Returns 200', function (done) {
    request(app)
      .get('/store')
      .expect(200, done);
  });

  it('Returns HTML', function (done) {
    request(app)
      .get('/store')
      .expect('Content-Type', /html/, done);
  });

  it('Returns expected content', function (done) {
    request(app)
      .get('/store')
      .expect(/Token Store Front/, done);
  });
});

describe('test /login path', function () {
  it('Returns 200', function (done) {
    request(app)
      .get('/login')
      .expect(200, done);
  });

  it('Returns HTML', function (done) {
    request(app)
      .get('/login')
      .expect('Content-Type', /html/, done);
  });

  it('Returns Facebook Social login button', function (done) {
    request(app)
      .get('/login')
      .expect(/fb-btn/, done);
  });

  it('Returns Google Social login button', function (done) {
    request(app)
      .get('/login')
      .expect(/google-btn/, done);
  });
});
