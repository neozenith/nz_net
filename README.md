# NeoZenith Website

[![build status](https://gitlab.com/neozenith/nz_net/badges/master/build.svg)](https://gitlab.com/neozenith/nz_net/commits/master)

[![coverage report](https://gitlab.com/neozenith/nz_net/badges/master/coverage.svg)](https://gitlab.com/neozenith/nz_net/commits/master)


Website used for portfolio purposes.

- WebGL Demos
- Contact Info
- Web Development

# Development

Uses Amazon Elastic Beanstalk for deployment.

```bash
# deploy to production
git checkout master
eb deploy

#Deploy to staging
git checkout staging
eb deploy
```

## Getting started

```bash
git clone https://gitlab.com/neozenith/nz_net.git ~/nz_net/
cd nz_net
eb init # Follow prompts
```

`.ebextensions` folder describes the CloudFormation and provisioning scripts. 

