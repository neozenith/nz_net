var express = require('express');
var router = express.Router();

/* GET main page. */
router.get('/', function (req, res, next) {
  res.render('chat', { title: 'WebGL Experiments' });
});

module.exports = router;
