var express = require('express');
var router = express.Router();

/* GET main demo page. */
router.get('/', function (req, res, next) {
  res.render('webgl', { title: 'WebGL Experiments' });
});

/* GET texture demo page. */
router.get('/texture', function (req, res, next) {
  res.render('webgl-texture', { title: 'WebGL Experiments' });
});

/* GET multiplayer demo page. */
router.get('/multiplayer', function (req, res, next) {
  res.render('webgl-multiplayer', { title: 'WebGL Experiments' });
});

module.exports = router;
